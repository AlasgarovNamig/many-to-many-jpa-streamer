package az.ingress.ManyToManyThirdTableAndJpaStream.dto;

import az.ingress.ManyToManyThirdTableAndJpaStream.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDto  {
    private  String name;
public  static  ProductDto fromEntity(Product product){
    return ProductDto.builder().name(product.getName()).build();
}

}
