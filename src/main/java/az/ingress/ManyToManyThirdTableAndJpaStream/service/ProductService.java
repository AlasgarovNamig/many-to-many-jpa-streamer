package az.ingress.ManyToManyThirdTableAndJpaStream.service;

import az.ingress.ManyToManyThirdTableAndJpaStream.dto.Dto;
import az.ingress.ManyToManyThirdTableAndJpaStream.dto.ProductDto;
import az.ingress.ManyToManyThirdTableAndJpaStream.model.Product;
import com.speedment.jpastreamer.application.JPAStreamer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.Entity;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductService {
    private final JPAStreamer jpaStreamer;

    public List<ProductDto> findAll() {
        return jpaStreamer.stream(Product.class)
                .map(ProductDto::fromEntity)
                .collect(Collectors.toList());
    }


//    private Dto convertEntityToDto(Entity entity){
//        return ProductDto.builder().name(entity.name()).build();
//    }

}
