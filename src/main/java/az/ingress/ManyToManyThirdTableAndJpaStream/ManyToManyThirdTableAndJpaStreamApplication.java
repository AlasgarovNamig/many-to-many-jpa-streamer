package az.ingress.ManyToManyThirdTableAndJpaStream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManyToManyThirdTableAndJpaStreamApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManyToManyThirdTableAndJpaStreamApplication.class, args);
	}

}
