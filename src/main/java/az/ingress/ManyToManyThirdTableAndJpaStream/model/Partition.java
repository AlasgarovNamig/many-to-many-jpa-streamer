package az.ingress.ManyToManyThirdTableAndJpaStream.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = Partition.TABLE_NAME)

public class Partition {
    public static final String TABLE_NAME = "partition_";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "partition_name")
    private String partition_name;

    @OneToMany(mappedBy = "partition")//, cascade = CascadeType.ALL
    private Set<PartitionProduct> partitionProducts = new HashSet<>();

}
